from django.apps import AppConfig


class FilterCsvConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'filter_csv'
