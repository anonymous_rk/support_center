import csv
import json

all_lst = []
loc = "../pp-complete.csv"
json_row = open("pp_complete.json", "w")
json_row.write("[\n")
with open(loc, 'r') as csv_file:
    spam_reader = csv.reader(csv_file, delimiter=" ")
    result = {}
    for row in csv_file:
        row = row.replace(", ", " ")
        row_split = row.split(",")
        for i in range(len(row_split)):
            row_split[i] = row_split[i].replace('"', "").replace("\n", "")

        if row_split[7] not in all_lst:
            all_lst.append(row_split[7])
        else:
            result = {
                "Transaction ID": row_split[0],
                "Price": row_split[1],
                "Date": row_split[2],
                "Postcode": row_split[3],
                # "Property Type": row_split[4],
                # "Old/New": row_split[5],
                # "Duration": row_split[6],
                "PAON": row_split[7],
                # "SAON": row_split[8],
                # "Street": row_split[9],
                # "Locality": row_split[10],
                # "Town/City": row_split[11],/
                # "District": row_split[12],
                # "Country": row_split[13],
                # "PPD Category": row_split[14],
                # "Record Status": row_split[15]
            }

        json_row.write("    " + json.dumps(result) + ',' + '\n')


json_row.write("]")
json_row.close()