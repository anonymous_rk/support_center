from django.db import models


class RealProperty(models.Model):

    transaction_id = models.CharField(max_length=40)
    price = models.CharField(max_length=10)
    date = models.CharField(max_length=20)
    postcode = models.CharField(max_length=10)
    property_type = models.CharField(max_length=2)
    old_new = models.CharField(max_length=2)
    duration = models.CharField(max_length=2)
    paon = models.CharField(max_length=30)
    saon = models.CharField(max_length=20)
    street = models.CharField(max_length=20)
    locality = models.CharField(max_length=20)
    town_city = models.CharField(max_length=20)
    district = models.CharField(max_length=20)
    country = models.CharField(max_length=50)
    ppd_category = models.CharField(max_length=2)
    record_status = models.CharField(max_length=2)

    class Meta:
        verbose_name = "Real Property"
        verbose_name_plural = "Real Properties"
