from django.urls import path, include
from rest_framework.routers import DefaultRouter
from .views import UserRequestsAPIView, SupportEmployeeAPIView, AssignUserEmployee

urlpatterns = [
    # path('', include())
    path('user_requests/', UserRequestsAPIView.as_view(), name='get-user-requests'),
    path('support_employees/', SupportEmployeeAPIView.as_view(), name='get-employees'),
    path('assign_request/', AssignUserEmployee.as_view(), name='assign-request'),
]
