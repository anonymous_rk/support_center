from .models import UserRequests, SupportEmployees
import json
from .serializers import UserRequestSerializer, SupportEmployeesSerializer
from django.shortcuts import render, get_object_or_404
from rest_framework.views import APIView
from rest_framework.response import Response
from django.http import Http404
from rest_framework.status import HTTP_200_OK, HTTP_201_CREATED, HTTP_400_BAD_REQUEST, HTTP_202_ACCEPTED


class UserRequestsAPIView(APIView):
    """
    User Requests API View
    """
    model = UserRequests
    serializer = UserRequestSerializer

    def get_object(self, pk):
        try:
            return self.model.objects.get(pk=pk)
        except self.model.DoesNotExist:
            return Http404

    def get(self, request, *args, **kwargs):
        user_requests = self.model.objects.all()
        serializer = UserRequestSerializer(user_requests, many=True)
        return Response(serializer.data)

    def post(self, request, *args, **kwargs):
        requests_serializer = self.serializer(data=request.data)
        if requests_serializer.is_valid():
            requests_serializer.save()
            return Response("User request created", status=HTTP_201_CREATED)
        return Response(requests_serializer.errors, status=HTTP_400_BAD_REQUEST)

    def put(self, request, *args, **kwargs):
        pk = request.query_params["id"]
        instance = self.get_object(pk)
        serializer = self.serializer(instance, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response("User request updated", status=HTTP_200_OK)
        return Response(serializer.errors, status=HTTP_400_BAD_REQUEST)


class SupportEmployeeAPIView(APIView):
    """
    Support Employee API View
    """
    model = SupportEmployees
    serializer = SupportEmployeesSerializer

    def get_object(self, pk):
        try:
            return self.model.objects.get(pk=pk)
        except self.model.DoesNotExist:
            return Http404

    def get(self, request, *args, **kwargs):
        user_requests = self.model.objects.all()
        return Response(user_requests)

    def post(self, request, *args, **kwargs):
        requests_serializer = self.serializer(data=request.data)
        if requests_serializer.is_valid():
            requests_serializer.save()
            return Response("User request created", status=HTTP_201_CREATED)
        return Response(requests_serializer.errors, status=HTTP_400_BAD_REQUEST)

    def put(self, request, *args, **kwargs):
        pk = request.query_params["id"]
        instance = self.get_object(pk)
        serializer = self.serializer(instance, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response("User request updated", status=HTTP_200_OK)
        return Response(serializer.errors, status=HTTP_400_BAD_REQUEST)


class AssignUserEmployee(APIView):
    """
    Link the Support Employee to the first User in the requests queue
    """
    model = UserRequests

    def get(self, request, *args, **kwargs):
        try:
            support_employee_id = request.data["support_employee_id"]
            waiting_request = self.model.objects.filter(status="Waiting").order_by('queue_number')[0]
            if support_employee_id and waiting_request:
                updated_request = UserRequests.objects.filter(id=waiting_request.id).update(
                    status="Respond",
                    responded=support_employee_id
                )
                updated_support_emp = SupportEmployees.objects.filter(id=support_employee_id).update(
                    is_free=False
                )

                return Response({
                    "status": HTTP_202_ACCEPTED,
                    "msg": f"Support employee num-{support_employee_id} is responding user from phone number-{UserRequests.objects.get(id=waiting_request.id).phone}"
                })
        except BaseException as e:
            return Response({
                "msg": e.args[0],
                "status": HTTP_400_BAD_REQUEST
            })



