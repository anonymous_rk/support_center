from django.contrib import admin
from django.contrib.auth.models import User
from .models import SupportEmployees, UserRequests


class UserRequestsAdmin(admin.ModelAdmin):
    list_display = "__all__"


class SupportEmployeesAdmin(admin.ModelAdmin):
    list_display = '__all__'


admin.site.register(UserRequests)
admin.site.register(SupportEmployees)