from django.db import models
from django.contrib.auth.models import User


class SupportEmployees(models.Model):
    """
    Support employees model
    """
    full_name = models.CharField(max_length=50)
    phone_num = models.CharField(max_length=20)
    unique_id = models.IntegerField()
    is_free = models.BooleanField(default=True)

    class Meta:
        verbose_name = "Support employee"
        verbose_name_plural = "Support employees"


REQUEST_STATUS = (
    ("Responded", "Responded"),
    ("Waiting", "Waiting"),
    ("Finished", "Finished"),
)


class UserRequests(models.Model):
    """
    Requests model got from Users and which Support employee responded to them
    """
    username = models.ForeignKey(User, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_created=True)
    updated_at = models.DateTimeField(auto_now_add=True)
    responded = models.ForeignKey(SupportEmployees, on_delete=models.CASCADE, blank=True, null=True)
    status = models.CharField(max_length=10, choices=REQUEST_STATUS, default="Waiting")
    finished_at = models.DateTimeField(null=True, blank=True)
    queue_number = models.IntegerField(default=0)
    phone = models.CharField(max_length=20, default="+998711234567")

    class Meta:
        verbose_name = 'User request'
        verbose_name_plural = 'User requests'

