from .models import UserRequests, SupportEmployees
from rest_framework.serializers import ModelSerializer


class UserRequestSerializer(ModelSerializer):

    class Meta:
        model = UserRequests
        fields = [
            'username',
            'created_at',
            'updated_at',
            'responded',
            'status',
            'finished_at',
            'queue_number',
            'phone',
        ]

    def create(self, validated_data):
        created = UserRequests.objects.create(validated_data)
        return created

    def update(self, instance, validated_data):
        updated = UserRequests.objects.update(instance, validated_data)
        return updated


class SupportEmployeesSerializer(ModelSerializer):
    model = SupportEmployees

    class Meta:
        model = SupportEmployees
        fields = [
            'full_name',
            'phone_num',
            'unique_id',
            'is_free',
        ]

    def create(self, validated_data):
        created = self.model.objects.create(validated_data)
        return created

    def update(self, instance, validated_data):
        updated = self.model.objects.update(instance, validated_data)
        return updated

