from django import forms
from .models import SupportEmployees, UserRequests


class SupportEmployeesForm(forms.ModelForm):
    class Meta:
        model = SupportEmployees
        fields = "__all__"


class UserRequestsForm(forms.ModelForm):
    class Meta:
        model = UserRequests
        fields = "__all__"
