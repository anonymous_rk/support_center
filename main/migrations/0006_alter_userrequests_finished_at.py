# Generated by Django 4.0.2 on 2022-02-25 11:40

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0005_alter_userrequests_responded'),
    ]

    operations = [
        migrations.AlterField(
            model_name='userrequests',
            name='finished_at',
            field=models.DateTimeField(blank=True, null=True),
        ),
    ]
