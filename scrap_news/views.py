from bs4 import BeautifulSoup
import requests
from datetime import datetime
import asyncio
from telethon import TelegramClient, events, utils, types
from .scrapper import scrapper


api_id = API_ID
api_hash = 'API_HASH'

real_id, peer_type = utils.resolve_id(CHANNEL_ID)  # -1002163642791


async def main(post_dict):
    client = TelegramClient('session_name', api_id, api_hash)
    await client.start()
    await client.send_message(
        types.PeerChannel(channel_id=CHANNEL_ID),  # peer_type(real_id) -> PeerChannel(channel_id=CHANNEL_ID)
        f"{post_dict['content_title']}\n {post_dict['content_text']}\n\n {post_dict['time']}"
    )


lst = scrapper()

for i in lst:
    asyncio.run(main(i))

