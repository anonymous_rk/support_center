from bs4 import BeautifulSoup
import requests
from datetime import datetime

today = datetime.now().strftime("%d.%m.%Y")


def scrapper():
    response = requests.get("https://vc.ru/new")

    soup = BeautifulSoup(response.text, "html.parser")

    news = []

    data = soup.find('div', attrs={"class": "layout__content l-relative"}).find('div', attrs={"class": "feed"}).findAll(
        'div', attrs={"class": "feed__item l-island-round"})

    ln = len(data)

    for i in range(1, ln):
        base_data = soup.find('div', attrs={"data-gtm": f"Feed — Item {i} — Click"})
        post_time = data[i].find('time', attrs={"class": "time"}).get("title").strip("\n ")
        header_data = data[i].find('div', attrs={
            "class": "content-header content-header--short content-header--anon l-island-a"}).find('div', attrs={
            "class": "content-header__info"}).findAll('a')
        category = header_data[0].find('div', attrs={"class": "content-header-author__name"})
        author = header_data[1].find('div', attrs={"class": "content-header-author__name"})
        content = data[i].find('div', attrs={"class": "content content--short"}).findAll('div', attrs={"class": "l-island-a"})
        if post_time[:2] == today[:2] and post_time[3:5] == today[3:5]:
            category = category.text.replace(' ', '').strip("\n") if category is not None else None
            author = author.text.replace(' ', '').strip("\n") if author is not None else None
            content_title = None
            content_text = None
            if len(content) >= 2:
                content_title = content[0].text.split('\n')[1].strip(" ")
                content_text = content[1].text.split('\n')[1].strip(" ")
            else:
                content_title = content[0].text.split('\n')[1].strip(" ")
            dct = {
                "category": category,
                "author": author,
                "time": post_time,
                "content_title": content_title,
                "content_text": content_text
            }
            news.append(dct)

    return news
